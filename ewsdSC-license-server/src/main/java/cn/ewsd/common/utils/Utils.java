package cn.ewsd.common.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Utils {

    public static String UUIDGenerator() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-", "").toUpperCase();
    }

    public static Date addDay(int num, Date Date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Date);
        calendar.add(Calendar.DATE, num);// 把日期往后增加 num 天.整数往后推,负数往前移动
        return calendar.getTime(); // 这个时间就是日期往后推一天的结果
    }

}
