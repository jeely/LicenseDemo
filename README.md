# LicenseDemo

#### 项目介绍
在基于Spring的项目中使用 `TrueLicense `生成和验证`License证书`（服务器许可）的示例代码

#### 技术依赖：
* `Spring Boot`：项目基础架构
* `TrueLicense `：基于`Java`实现的生成和验证服务器许可的简单框架

#### 环境依赖：
* `JDK8+`

#### 两个子项目说明： ####

- `ServerDemo`：用于**开发者**给客户生成`License证书`的示例代码
- `ClientDemo`：**模拟需要给客户部署的业务项目**

#### ServerDemo项目： ####

对外发布了两个RESTful接口：

（1）获取服务器硬件信息 ：

请求地址：`http://127.0.0.1:7000/license/getServerInfos`

![获取服务器硬件信息](https://www.zifangsky.cn/wp-content/uploads/2018/07/20180710140711.png)

（2）生成证书 ：

请求地址：`http://127.0.0.1:7000/license/generateLicense`

发送PUT请求时需要在Body中添加一个 **raw** ，格式为：**JSON(application/json)**。请求参数如下： 

```json
{
    "subject": "zysdEMIS",
    "privateAlias": "privateKey",
    "keyPass": "ewsd04500329",
    "storePass": "ewsd19851013",
    "licensePath": "D:/zysd/lic/emis/new/license.lic",
    "privateKeysStorePath": "D:/license/server/privateKeys.keystore",
    "issuedTime": "2018-08-18 18:08:18",
    "expiryTime": "2019-08-18 18:08:18",
    "consumerType": "User",
    "consumerAmount": 1,
    "description": "EMIS快速开发平台微服务版",
    "licenseCheckModel": {
        "ipAddress": [
            "192.168.0.168",
            "10.0.75.1",
            "172.22.226.113"
        ],
        "macAddress": [
            "30-9C-23-B1-E1-84",
            "00-15-5D-00-A8-02",
            "BE-15-C4-95-BF-AD"
        ],
        "cpuSerial": "BFEBFBFF000906EA",
        "mainBoardSerial": "I416656825"
    }
}
```

![生成证书](https://www.zifangsky.cn/wp-content/uploads/2018/07/20180710141528.png)

#### ClientDemo项目： ####

项目启动时安装证书，通过`cn/zifangsky/license/LicenseCheckListener.java`类实现。用户登录时校验证书的可用性，通过`cn/zifangsky/license/LicenseCheckInterceptor.java`类实现。

#### 特别说明： ####

详细开发思路可以参考我写的这篇文章：[https://www.zifangsky.cn/1277.html](https://www.zifangsky.cn/1277.html)

